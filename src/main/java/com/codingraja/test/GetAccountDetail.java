package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Account;
import com.codingraja.domain.CurrentAccount;
import com.codingraja.domain.LoanAccount;
import com.codingraja.domain.SavingAccount;

public class GetAccountDetail {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		Account acc = session.get(Account.class, new Long(1));
		SavingAccount savingAccount = session.get(SavingAccount.class, new Long(2));
		CurrentAccount currentAccount = session.get(CurrentAccount.class, new Long(3));
		LoanAccount loanAccount = session.get(LoanAccount.class, new Long(4));
		session.close();
		
		System.out.println("Account Balane: "+acc.getBalance());
		System.out.println("Saving Account Balane: "+savingAccount.getBalance());
		System.out.println("Current Account Balane: "+currentAccount.getBalance());
		System.out.println("Loan Account Balane: "+loanAccount.getBalance());
	}

}
