package com.codingraja.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="saving_account_master2")
public class SavingAccount extends Account {
	@Column(name="inrest_rate")
	private Double intrestRate;
	
	public SavingAccount(){}
	public SavingAccount(Double balance, Long customerId, Double intrestRate) {
		super(balance, customerId);
		this.intrestRate = intrestRate;
	}
	
	public Double getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(Double intrestRate) {
		this.intrestRate = intrestRate;
	}
}
